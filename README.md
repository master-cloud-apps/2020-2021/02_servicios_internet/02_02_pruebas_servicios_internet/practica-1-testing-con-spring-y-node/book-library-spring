# Book Library

Proyecto con una librería online escrito en Java con Spring.

## Test unitarios

Implementados de la siguiente forma:

* Unitarios: Bajo `src/test/java/es/urjc/code/daw/library/rest`
* Unitarios con `WebTestClient`: Bajo `src/test/java/es/urjc/code/daw/library/rest/wtc`

Para lanzar los tests unitarios lanzaremos:

````shell script
mvn clean verify
````

## Test de Integración

Implementados de la siguiente forma:

* Con Rest Assured: Bajo `src/test-integration/java/es/urjc/code/daw/library/rest/assured`
* Unitarios con `WebTestClient`: Bajo `src/test-integration/java/es/urjc/code/daw/library/rest/wtc`


Para lanzar los tests unitarios junto con los tests de integración lanzaremos:

````shell script
mvn clean verify -Pfailsafe
````