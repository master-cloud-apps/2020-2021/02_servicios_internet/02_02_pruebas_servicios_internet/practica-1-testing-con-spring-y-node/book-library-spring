package es.urjc.code.daw.library.rest;

import es.urjc.code.daw.library.book.Book;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static es.urjc.code.daw.library.book.TestConstants.*;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BookControllerAdminTest extends ControllerUnitTest {

    @Test
    @WithMockUser(username = ADMIN_NAME, password = ADMIN_PASS, roles = {ADMIN_ROLE, USER_ROLE})
    void givenUserWithRoleAdminLogged_whenDeleteBook_shouldReturnOK() throws Exception {
        Book newBook = this.createBook();

        this.mockMvc.perform(delete(API_BOOK_URL + newBook.getId()))
                .andExpect(status().isOk());

        this.mockMvc.perform(get(API_BOOK_URL + newBook.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = ADMIN_NAME, password = ADMIN_PASS, roles = {ADMIN_ROLE, USER_ROLE})
    void givenUserWithRoleAdminLogged_whenCreateBook_shouldReturnOK() throws Exception {
        Book newBook = new Book("Cien años de Soledad", "Libro de Gabo");

        MvcResult result = this.mockMvc.perform(
                post("/api/books/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(newBook)))
                .andExpect(status().isCreated())
                .andReturn();

        Book responseBook = objectMapper.readValue(result.getResponse().getContentAsString(), Book.class);
        this.mockMvc.perform(get(API_BOOK_URL + responseBook.getId()))
                .andExpect(status().isOk());

        this.mockMvc.perform(delete(API_BOOK_URL + responseBook.getId()));
    }

    @Test
    @WithMockUser(username = ADMIN_NAME, password = ADMIN_PASS, roles = {ADMIN_ROLE, USER_ROLE})
    void givenUserWithRoleAdminLogged_whenGetAllBooks_shouldReturnDefaultBooks() throws Exception {
        this.mockMvc.perform(get(API_BOOK_URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }
}
