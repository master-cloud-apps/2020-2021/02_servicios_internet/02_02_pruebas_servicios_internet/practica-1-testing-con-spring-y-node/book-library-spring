package es.urjc.code.daw.library.book;

import es.urjc.code.daw.library.book.Book;
import es.urjc.code.daw.library.book.BookRepository;
import org.mockito.stubbing.Answer;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

public class TestPreparation {

    public void prepareMocks(BookRepository bookRepository, BookRepository bookFakeRepository){
        when(bookRepository.findAll()).thenReturn(bookFakeRepository.findAll());
        when(bookRepository.save(any())).then((Answer<Book>) invocationOnMock -> bookFakeRepository.save(invocationOnMock.getArgument(0)));
        when(bookRepository.findById(any())).then((Answer<Optional<Book>>) invocationOnMock -> bookFakeRepository.findById(invocationOnMock.getArgument(0)));
        doAnswer((Answer<Void>) invocationOnMock -> {
            bookFakeRepository.deleteById(invocationOnMock.getArgument(0));
            return null;
        }).when(bookRepository).deleteById(any());
    }
}
