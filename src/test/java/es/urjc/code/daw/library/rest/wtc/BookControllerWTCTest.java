package es.urjc.code.daw.library.rest.wtc;

import es.urjc.code.daw.library.book.Book;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static es.urjc.code.daw.library.book.TestConstants.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookControllerWTCTest extends WTCTest {

    @Test
    void givenNoRoleUser_whenGetBooks_shouldReturnAllBooks() {
        this.wtc.get().uri(API_BOOK_URL).exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$").value(hasSize(5));
    }

    @Test
    void givenNoRoleUser_whenDeleteBook_shouldReturnNotAllowed() {
        this.wtc.delete().uri(API_BOOK_URL + 2).exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    @WithMockUser(username = USER_NAME, password = USER_PASS, roles = USER_ROLE)
    void givenLoggedUser_whenDeleteBook_shouldReturnNotAllowed() {
        this.wtc.delete().uri(API_BOOK_URL + 2).exchange()
                .expectStatus().isForbidden();
    }

    @Test
    @WithMockUser(username = USER_NAME, password = USER_PASS, roles = USER_ROLE)
    void givenLoggedUser_whenCreateBook_shouldReturnOK() {
        this.wtc.post()
                .uri(API_BOOK_URL)
                .bodyValue(new Book("La vida de Alvaro", "Las cosas que le pasan"))
                .exchange()
                .expectStatus().isCreated()
                .expectBody()
                .jsonPath("$.title").value(equalTo("La vida de Alvaro"))
                .jsonPath("$.description").value(equalTo("Las cosas que le pasan"))
                .jsonPath("$.id").value(equalTo(5));

        assertEquals(6, this.bookFakeRepository.findAll().size());

        this.bookFakeRepository.deleteById(5L);
    }


    @Test
    @WithMockUser(username = ADMIN_NAME, password = ADMIN_PASS, roles = {ADMIN_ROLE, USER_ROLE})
    void givenAdminUser_whenDeleteBook_shouldReturnOKandNoBookFound() {
        this.wtc.get().uri(API_BOOK_URL + 2).exchange()
                .expectStatus().isOk();

        this.wtc.delete().uri(API_BOOK_URL + 2).exchange()
                .expectStatus().isOk();

        this.wtc.get().uri(API_BOOK_URL + 2).exchange()
                .expectStatus().isNotFound();
    }
}
