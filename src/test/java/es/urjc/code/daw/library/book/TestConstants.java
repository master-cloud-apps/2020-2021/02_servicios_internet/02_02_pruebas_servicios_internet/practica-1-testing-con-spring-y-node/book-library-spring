package es.urjc.code.daw.library.book;

public class TestConstants {

    private TestConstants() {
    }

    public static final String ADMIN_NAME = "admin";
    public static final String ADMIN_PASS = "pass";
    public static final String ADMIN_ROLE = "ADMIN";
    public static final String USER_NAME = "user";
    public static final String USER_PASS = "pass";
    public static final String USER_ROLE = "USER";
    public static final String API_BOOK_URL = "/api/books/";
}
