package es.urjc.code.daw.library.rest.wtc;

import es.urjc.code.daw.library.book.BookFakeRepository;
import es.urjc.code.daw.library.book.BookRepository;
import es.urjc.code.daw.library.book.TestPreparation;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.client.MockMvcWebTestClient;

@SpringBootTest
@AutoConfigureMockMvc
public abstract class WTCTest {

    @Autowired
    protected MockMvc mockMvc;
    @MockBean
    protected BookRepository bookRepository;

    protected BookFakeRepository bookFakeRepository = new BookFakeRepository();

    protected WebTestClient wtc;
    private TestPreparation testPreparation;

    public WTCTest() {
        this.testPreparation = new TestPreparation();
    }

    @BeforeEach
    void setUp(){
        this.wtc = MockMvcWebTestClient.bindTo(mockMvc).build();
        this.testPreparation.prepareMocks(bookRepository, bookFakeRepository);
    }
}
