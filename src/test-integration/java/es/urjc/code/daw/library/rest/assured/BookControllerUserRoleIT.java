package es.urjc.code.daw.library.rest.assured;

import es.urjc.code.daw.library.book.Book;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.server.LocalServerPort;

import static es.urjc.code.daw.library.book.TestConstants.*;
import static io.restassured.RestAssured.given;

class BookControllerUserRoleIT extends RestAssuredConfigurationIT {

    @LocalServerPort
    int port;

    @Test
    void givenUserWithRoleUserLogged_whenCreateBook_shouldReturnOK() {
        Book result = this.createBook(USER_NAME, USER_PASS);

        this.setUp();
        given()
                .auth()
                .basic(ADMIN_NAME, ADMIN_PASS)
                .when().delete(API_BOOK_URL + result.getId())
                .then().statusCode(200);

    }

}
