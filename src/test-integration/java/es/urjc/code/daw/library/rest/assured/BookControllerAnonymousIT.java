package es.urjc.code.daw.library.rest.assured;

import es.urjc.code.daw.library.book.Book;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.Arrays;
import java.util.List;

import static es.urjc.code.daw.library.book.TestConstants.API_BOOK_URL;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BookControllerAnonymousIT extends RestAssuredConfigurationIT {

    @LocalServerPort
    int port;

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
        RestAssured.useRelaxedHTTPSValidation();
        RestAssured.baseURI = "https://localhost:" + port;
    }

    @Test
    void getAllBooks() {
        List<Book> bookList = Arrays.asList(given().
                when().
                get(API_BOOK_URL).
                then().
                statusCode(200)
                .extract().as(Book[].class));

        assertEquals(5, bookList.size());
    }

}
