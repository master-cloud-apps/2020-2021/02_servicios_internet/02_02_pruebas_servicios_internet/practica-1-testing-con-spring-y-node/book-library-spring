package es.urjc.code.daw.library.rest.wtc;

import es.urjc.code.daw.library.book.BookService;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.netty.http.client.HttpClient;

import javax.net.ssl.SSLException;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class WTCIT {

    protected WebTestClient wtc;

    @LocalServerPort
    private int port;

    @Autowired
    protected BookService bookService;

    @BeforeEach
    public void setup() throws SSLException {
        SslContext sslContext = SslContextBuilder
                .forClient()
                .trustManager(InsecureTrustManagerFactory.INSTANCE)
                .build();
        HttpClient httpClient = HttpClient.create()
                .secure(sslSpec -> sslSpec.sslContext(sslContext))
                .baseUrl("https://localhost:" + port);
        ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);
        this.wtc = WebTestClient
                .bindToServer(connector)
                .build();
    }

}
