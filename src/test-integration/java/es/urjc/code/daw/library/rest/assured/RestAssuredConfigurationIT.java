package es.urjc.code.daw.library.rest.assured;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.urjc.code.daw.library.book.Book;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static es.urjc.code.daw.library.book.TestConstants.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class RestAssuredConfigurationIT {

    @LocalServerPort
    int port;

    @Autowired
    protected ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        RestAssured.port = port;
        RestAssured.useRelaxedHTTPSValidation();
        RestAssured.baseURI = "https://localhost:" + port;
    }

    protected Book createBook(String userName, String userPass){
        Book result = given()
                .auth()
                .basic(userName, userPass)
                .body(new Book("test_name", "test_description"))
                .contentType("application/json")
                .when()
                .post(API_BOOK_URL)
                .then()
                .statusCode(201)
                .body("title", equalTo("test_name"))
                .extract()
                .as(Book.class);
        return result;
    }

}
